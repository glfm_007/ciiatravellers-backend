<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcademicInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_information', function (Blueprint $table) {
            $table->id();
            $table->string('university');
            $table->string('building_name');
            $table->string('college_career');
            $table->date('start_date');
            $table->string('period_type');
            $table->string('current_period');
            $table->string('current_subjects')->nullable();
            $table->string('last_period_subjects')->nullable();
            $table->string('next_period_subjects')->nullable();
            $table->string('carreer_subjects')->nullable();
            $table->string('general_academic_index');
            $table->string('expected_end_date')->nullable();
            $table->string('career_selection_reason')->nullable();
            $table->string('post_career_reason')->nullable();
            $table->string('career_interruption')->nullable();
            $table->string('career_change')->nullable();
            $table->string('university_change')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academic_information');
    }
}
