<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personal_information', function($table)
        {
            $table->unsignedBigInteger('passport_id')->after('country');
            $table->foreign('passport_id')->references('id')->on('passport_information');
            
            $table->unsignedBigInteger('visa_id')->after('passport_id');
            $table->foreign('visa_id')->references('id')->on('visa_information');
        });

        Schema::table('lenguage_level_information', function($table)
        {
            $table->unsignedBigInteger('lenguage_id')->after('conversation_level');
            $table->foreign('lenguage_id')->references('id')->on('lenguage');
        });

        // Schema::table('visa_information', function($table)
        // {
        //     $table->unsignedBigInteger('country_id')->after('participation_year');
        //     $table->foreign('country_id')->references('id')->on('countries');
        // });

        // Schema::table('family_information', function($table)
        // {
        //     $table->unsignedBigInteger('country_id')->after('visa');
        //     $table->foreign('country_id')->references('id')->on('countries');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
