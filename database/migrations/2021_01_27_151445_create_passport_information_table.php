<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePassportInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passport_information', function (Blueprint $table) {
            $table->id();
            $table->string('passport_type');
            $table->string('no_passport');
            $table->string('passport_reason');
            $table->date('passport_birthday');
            $table->date('issue_date');
            $table->date('due_date');
            $table->string('birth_place');
            $table->string('issuing_authority');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passport_information');
    }
}
