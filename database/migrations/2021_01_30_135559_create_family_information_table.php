<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamilyInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family_information', function (Blueprint $table) {
            $table->id();
            $table->string('father_information');
            $table->string('mother_information');
            $table->string('tutor_information');
            $table->string('outliving_information');
            $table->string('relationship');
            $table->string('first_name');
            $table->string('second_name')->nullable();
            $table->string('first_last_name');
            $table->string('second_last_name')->nullable();
            $table->date('birthday');
            $table->string('cell_phone');
            $table->string('contact_number')->nullable();
            $table->string('email');
            $table->string('paying');
            $table->string('visa')->nullable();
            $table->string('residence_country');
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('migratory_status')->nullable();
            $table->string('time_residing_abroad')->nullable();
            $table->string('province');
            $table->string('municipality');
            $table->string('street_name');
            $table->string('sector');
            $table->string('building');
            $table->string('house_no');
            $table->string('currently_working');
            $table->string('job_type');
            $table->string('salary_currency');
            $table->string('salary');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family_information');
    }
}
