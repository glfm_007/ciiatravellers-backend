<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_information', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('second_name')->nullable();
            $table->string('first_last_name');
            $table->string('second_last_name')->nullable();
            $table->date('birth_day');
            $table->string('document_type');
            $table->string('identity_id');
            $table->string('gender');
            $table->string('country');
            $table->string('branch_office');
            $table->string('house_phone')->nullable();
            $table->string('cell_phone');
            $table->string('street');
            $table->string('neighbordhood')->nullable();
            $table->string('residential')->nullable();
            $table->string('house_number');
            $table->string('province');
            $table->string('municipality');
            $table->string('birth_place');
            $table->string('shirt_size');
            $table->string('civil_status');
            $table->string('emergency_contact_name');
            $table->string('relations_type');
            $table->string('emergency_contact_phone');
            $table->string('facebook_profile')->nullable();
            $table->string('skype_account')->nullable();
            $table->string('instagram_account')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_information');
    }
}
