<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_information', function (Blueprint $table) {
            $table->id();
            $table->string('about_us');
            $table->string('health_condition')->nullable();
            $table->string('medication_prescription')->nullable();
            $table->string('english_learning')->nullable();
            $table->string('campus');
            $table->string('year');
            $table->string('siblings');
            $table->string('childrens');
            $table->string('own_house')->nullable();
            $table->string('can_swim');
            $table->string('weekends')->nullable();
            $table->string('program_expectations');
            $table->string('cultural_activities');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_information');
    }
}
