<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisaInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visa_information', function (Blueprint $table) {
            $table->id();
            $table->string('visa');
            $table->string('issue_country');
            $table->date('issue_date');
            $table->date('due_date');
            $table->string('agency');
            $table->string('participation_year');
            $table->string('state');
            $table->string('company');
            $table->string('operational_position');
            $table->string('city');
            $table->string('visited_country');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('trip_reason');
            $table->string('residence_request');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visa_information');
    }
}
